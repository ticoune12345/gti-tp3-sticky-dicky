package gti310.tp3;

import gti310.tp3.graphe.Graphes;
import gti310.tp3.graphe.GraphesChemin;
import gti310.tp3.parser.Parser;
import gti310.tp3.parser.ConcreteParser;
import gti310.tp3.solver.ConcreteSolver;
import gti310.tp3.solver.Solver;
import gti310.tp3.writer.ConcreteWriter;
import gti310.tp3.writer.Writer;

import java.util.LinkedList;

/**
 * The Application class defines a template method to call the elements to
 * solve the problem Unreal-Networks is facing.
 * 
 * @author Francois Caron <francois.caron.7@ens.etsmtl.ca>
 */
public class Application 
{
    private static final String FORMAT_FICHIER = ".txt";
    
	/**
	 * The Application's entry point.
	 * 
	 * The main method makes a series of calls to find a solution to the
	 * problem. The program awaits two arguments, the complete path to the
	 * input file, and the complete path to the output file.
	 * 
	 * @param args The array containing the arguments to the files.
	 */
	public static void main(String args[]) 
	{
	    try
	    {
	        if(args[0].equals("?"))
	        {
	           System.out.println("PLEASE PRESS <ENTER> TO CONTINUE...");
	            System.in.read();
	            System.exit(1);
	        }
	        else if(args.length != 2)
	        {
	            System.out.println("PARAMETERS INVALID!");
	            System.exit(1);
	        }
    		
    		String fichierSource = "";
    		String fichierSortant = "";
    		
    		if(!validationChemin(args[0]) || !validationChemin(args[1]))
    		{
    		    System.out.println("INVALID FORMAT!\nSUPPORTED FORMAT: *" + FORMAT_FICHIER);
    		    System.exit(1);
    		}
    		else
    		{
    		    fichierSource = args[0];
    		    fichierSortant = args[1];
    		}
    		 
    		Graphes grapheSource = null;
    		
    		Parser<Graphes> interpreteur = new ConcreteParser();
    		Solver<Graphes, LinkedList<GraphesChemin>> solveur = new ConcreteSolver();
    		Writer<LinkedList<GraphesChemin>> ecriveur = new ConcreteWriter();
    		
    		
    		grapheSource = interpreteur.parse(fichierSource);
    		LinkedList<GraphesChemin> resultat = solveur.solve(grapheSource);
    		ecriveur.write(fichierSortant, resultat);
	    }
	    catch(Exception e)
	    {
	        e.printStackTrace();
	    }
	}
	

	private static boolean validationChemin(String chemin)
	{
	    return chemin.endsWith(FORMAT_FICHIER);
	}
}