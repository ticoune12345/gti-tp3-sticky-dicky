package gti310.tp3.solver;

import java.util.LinkedList;

import gti310.tp3.graphe.Graphes;
import gti310.tp3.graphe.GraphesChemin;
import gti310.tp3.graphe.GraphesNoeud;

public class ConcreteSolver implements Solver<Graphes, LinkedList<GraphesChemin>> {

	private Graphes grapheEntrant;
	private LinkedList<GraphesChemin> chemins;
	
	/*
     * Analyse de complexité:
     * Soit n noeuds
     * un noeud n-1 voisins.
     * noeud n=1 ->  n-1 voisins possible
     * noeud n=2 ->, n-2 etc
     * Donc O(n!) chemins. DONE
     */
	
	public LinkedList<GraphesChemin> solve(Graphes entrant) {
		this.grapheEntrant = entrant;
		this.chemins = new LinkedList<GraphesChemin>();
		LinkedList<GraphesChemin> resultat = new LinkedList<GraphesChemin>();
		GraphesChemin chemin = new GraphesChemin();
		chemin.add(entrant.getNoeudDepart());
		passage(entrant.getNoeudDepart(), chemin);

		for(GraphesChemin ch : chemins)
		{
		    for(GraphesNoeud voisin : ch.getLast().getVoisins())
		    {
		        if(voisin.getNoeudID() == this.grapheEntrant.getNoeudDepart().getNoeudID())
		        {
		            resultat.add(ch);
		        }
		    }
		}
		    
		return resultat;
	}
	
	private void passage(GraphesNoeud enCours, GraphesChemin chemin) {
		
		for (GraphesNoeud voisin : enCours.getVoisins()) {
			if (!chemin.contains(voisin)) {
				chemin.add(voisin);
				// Récursivité
				passage(voisin, chemin);
			}
		}

		if (chemin.size() == grapheEntrant.getNombreNoeuds()) {
			//chemin trouvé
			this.chemins.add((GraphesChemin) chemin.clone());
		}
		
		// Récursivité
		chemin.removeLast();
	}
}