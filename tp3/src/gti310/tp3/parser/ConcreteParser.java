package gti310.tp3.parser;

import gti310.tp3.graphe.Graphes;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConcreteParser implements Parser<Graphes> {
	
	public Graphes parse(String nomFichier){
		Graphes graphe = null;
		try {
			BufferedReader fluxEntrant = new BufferedReader(new InputStreamReader(new FileInputStream(nomFichier)));
			int nombreNoeuds = Integer.parseInt(fluxEntrant.readLine());
			fluxEntrant.readLine();
			int departNoeudID = Integer.parseInt(fluxEntrant.readLine());
			graphe = new Graphes(nombreNoeuds, departNoeudID);
			String ligne;
			for (ligne = fluxEntrant.readLine(); ligne != null && !ligne.equals("$"); ligne = fluxEntrant.readLine()) {
				String[] bordure = ligne.split("\t");
				if (bordure.length != 3) {
					System.out.println("Data from file :" + nomFichier + " are invalid!!");
					System.exit(1);
				}
				
			
				int depart = Integer.parseInt(bordure[0]);
				int destination = Integer.parseInt(bordure[1]);
				Integer.parseInt(bordure[2]); 
				graphe.ajoutBordure(depart, destination);
			}
			if (ligne == null) {
			
				System.out.println("Data from file : " + nomFichier + "are invalid!!");
				System.exit(1);
			}
			fluxEntrant.close();
			
		
		} catch (FileNotFoundException e) {
			System.out.println("File: " + nomFichier + " doesn't exist!");
			System.exit(1);
		} catch (NumberFormatException e) {
			System.out.println("Data from file : " + nomFichier + "are invalid!!");
			System.exit(1);
		} catch (IOException e) {
			System.out.println("ERROR reading file soorryyyy: " + nomFichier);
			System.exit(1);
		}
		return graphe;
	}
}