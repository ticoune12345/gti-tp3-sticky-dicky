package gti310.tp3.writer;

import gti310.tp3.graphe.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

public class ConcreteWriter implements Writer<LinkedList<GraphesChemin>> 
{
    public void write(String nomFichier, LinkedList<GraphesChemin> resultats)
    {
        try 
        {
            BufferedWriter out = new BufferedWriter(new FileWriter(nomFichier));
            
            if (resultats.isEmpty()) {
            	System.out.println("Aucun résultats!");
            	out.write("Aucun Résultats!");
            } else {
	            for(GraphesChemin resulat : resultats)
	            {
	                System.out.println(resulat.toString());
	            	out.write(resulat.toString());
	                out.newLine();
	            }
            }
            
            out.close();
        } 
        catch (IOException e) 
        {
            System.out.println("ERROR writing file: " + nomFichier);
            System.exit(1);
        }
    } 
}