package gti310.tp3.graphe;


import java.util.Iterator;
import java.util.LinkedList;

public class GraphesChemin implements Cloneable {

	private LinkedList<GraphesNoeud> chemin = null;
	
	public GraphesChemin() {
		chemin = new LinkedList<GraphesNoeud>();
	}
	
	public GraphesChemin(LinkedList<GraphesNoeud> chemin) 
	{
	    this.chemin = chemin;
	}
	
	public void add(GraphesNoeud noeud) {
		this.chemin.add(noeud);
	}
	
	
	public boolean contains(GraphesNoeud voisin) {

		return this.chemin.contains(voisin)? true : false;
    }

    public int size() 
    {
        return this.chemin.size();
    }

    public void removeLast() 
    {
        this.chemin.removeLast();
    }
    
    public LinkedList<GraphesNoeud> getPath()
    {
        return this.chemin;
    }
    
    public GraphesNoeud getLast ()
    {
        return this.chemin.getLast();
    }
    
    @Override
    public String toString() 
    {
        String chaine = "";
        Iterator<GraphesNoeud> itt = this.chemin.iterator();
        while(itt.hasNext())
            chaine += itt.next().getNoeudID() + " ";
        
        chaine += this.chemin.getFirst().getNoeudID();
        return chaine;
    }
    
    public Object clone() 
    {
        GraphesChemin resultat = new GraphesChemin();
        for(GraphesNoeud noeud : this.chemin)
            resultat.add(noeud);
        return resultat;
    }
}