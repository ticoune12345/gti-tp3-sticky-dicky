package gti310.tp3.graphe;

import java.util.HashMap;

public class Graphes 
{
    private int nombreNoeuds = 0;
    private GraphesNoeud noeudDepart = null;
    private HashMap<Integer, GraphesNoeud> noeuds = null;
    
    public Graphes(int nombreNoeuds, int noeudDepartID) 
    {
        this.nombreNoeuds = nombreNoeuds;
        this.noeudDepart = new GraphesNoeud(noeudDepartID);
        this.noeuds = new HashMap<Integer, GraphesNoeud>();
        this.noeuds.put(noeudDepartID, noeudDepart);
    }
    
    public void ajoutBordure(int departID, int destinationID)
    {
    	GraphesNoeud depart;
    	if (noeuds.containsKey(departID)) {
    		depart = noeuds.get(departID);
    	} else {
    		depart = new GraphesNoeud(departID);
    		this.noeuds.put(departID, depart);
    	}
    	
    	GraphesNoeud destination;
    	if (noeuds.containsKey(destinationID)) {
    		destination = noeuds.get(destinationID);
    	} else {
    		destination = new GraphesNoeud(destinationID);
    		this.noeuds.put(destinationID, destination);
    	}
    	
    	depart.ajoutVoisin(destination);
    }
    
    public GraphesNoeud getNoeudDepart() {
    	return noeudDepart;
    }
    
    @Override
    public String toString() 
    {
        String s = "";
    	for (GraphesNoeud noeud : noeuds.values()) 
    	{
    		s += noeud.toString();
    	}
    	return s;
    }
    
    public int getNombreNoeuds() {
    	return nombreNoeuds;
    }
}
