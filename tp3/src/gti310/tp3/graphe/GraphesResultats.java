package gti310.tp3.graphe;


import java.util.Iterator;
import java.util.LinkedList;

public class GraphesResultats implements Cloneable {

	private LinkedList<GraphesNoeud> chemin = null;
	
	public GraphesResultats() {
		chemin = new LinkedList<GraphesNoeud>();
	}
	
	public GraphesResultats(LinkedList<GraphesNoeud> path) 
	{
	    this.chemin = path;
	}
	
	public void add(GraphesNoeud n) {
		this.chemin.add(n);
	}
	
	public boolean contains(GraphesNoeud neighbour) {
        
        return this.chemin.contains(neighbour)? true : false;
    }

    public int size() 
    {
        return this.chemin.size();
    }

    public void removeLast() 
    {
        this.chemin.removeLast();
    }
    
    public LinkedList<GraphesNoeud> getPath()
    {
        return this.chemin;
    }
    
    @Override
    public String toString() 
    {
        String s = "";
        Iterator<GraphesNoeud> itt = this.chemin.iterator();
        while(itt.hasNext())
            s += itt.next().getNoeudID() + " ";
        
        s += this.chemin.getFirst().getNoeudID();
        return s;
    }
    
    public Object clone() 
    {
        GraphesResultats solution = new GraphesResultats();
        for(GraphesNoeud node : this.chemin)
            solution.add(node);
        return solution;
    }
}