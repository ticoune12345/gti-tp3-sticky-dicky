package gti310.tp3.graphe;

import java.util.Collection;
import java.util.Vector;

public class GraphesNoeud
{
    private int noeudID = 0;
    private Vector<GraphesNoeud> voisins = null;
    private GraphesNoeud ancien = null;
    private int cheminLongueur = 0;
    private boolean dejaParcouru = false;
    
    public GraphesNoeud(int noeudID)
    {
        this.noeudID = noeudID;
        this.voisins = new Vector<GraphesNoeud>();
    }
    
    public void ajoutVoisin(GraphesNoeud noeud)
    {
        this.voisins.add(noeud);
    }
    
    @Override
    public String toString()
    {
    	String chaine = noeudID + " prédécesseur: " + ancien.getNoeudID() + " chemin: " + cheminLongueur + "\n";
   
    	return chaine;
    } 
    
    public int getNoeudID() 
    {
    	return noeudID;
    }
    
    public Collection<GraphesNoeud> getVoisins() {
    	return voisins;
    }
    
    public void setPredecessor(GraphesNoeud noeud) {
    	ancien = noeud;
    }
    
    public void setCheminLongueur(int longueur) {
    	cheminLongueur = longueur;
    }
    
    public int getCheminLongueur() {
    	return cheminLongueur;
    }
    
    public void setDejaParcouru(boolean dejaParcouru) {
    	this.dejaParcouru = dejaParcouru;
    }
    
    public boolean dejaParcouru() {
    	return dejaParcouru;
    }

}